package com.example.santesttask.extensions

import android.content.Intent
import android.net.Uri
import com.example.santesttask.view.MainActivity

fun MainActivity.startCall(phoneNumber: String){
    startActivity(Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phoneNumber")))
}

fun MainActivity.sendEmail(email: String, message: String) {
    val mailIntent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:$email")).also {
        it.putExtra(Intent.EXTRA_TEXT, message)
    }
    startActivity(Intent.createChooser(mailIntent, "Select a client"))
}

