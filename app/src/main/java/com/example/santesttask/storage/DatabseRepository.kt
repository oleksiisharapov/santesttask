package com.example.santesttask.storage

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.santesttask.model.Contact
import com.example.santesttask.model.ContactItem
import com.example.santesttask.storage.room.ContactDAO

class DatabaseRepository(private var contactDAO: ContactDAO, private val userId: String) : Repository {


    override suspend fun getContacts(): LiveData<List<Contact>> {
        return Transformations.map(contactDAO.getContactsWithItems(userId)) { list ->
            list.map { contactItem ->
                contactItem.contact.apply { contactList = contactItem.contactItems }
            }
        }
    }

    override suspend fun addContact(contact: Contact) {
        val genId = contactDAO.insertContact(contact.apply {
            userId = this@DatabaseRepository.userId
        })
        contact.contactList?.let {
            it.forEach { item ->
                contactDAO.insertContactItem(item.apply { contactId = genId.toInt() })
            }
        }
    }

    override suspend fun updateContact(contact: Contact) {
        contactDAO.updateContact(contact)
        contact.contactList?.let {
            it.forEach { item ->
                val operationResult = contactDAO.updateContactItem(item)
                if (operationResult == 0) {
                    contactDAO.insertContactItem(item)
                }
            }
        }
    }

    override suspend fun deleteContact(contact: Contact) {
        contactDAO.deleteContact(contact)
        contact.contactList?.let {
            it.forEach { item ->
                contactDAO.deleteContactItem(item)
            }
        }
    }

    override suspend fun addContactItem(contactItem: ContactItem) {
        contactDAO.insertContactItem(contactItem)
    }

    override suspend fun deleteContactItem(contactItem: ContactItem) {
        contactDAO.deleteContactItem(contactItem)
    }
}
