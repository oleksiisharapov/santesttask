package com.example.santesttask.storage.room

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.santesttask.model.Contact
import com.example.santesttask.model.ContactItem
import com.example.santesttask.model.ContactWithContactItems


@Dao
interface ContactDAO{

    @Query("SELECT * FROM contact WHERE userId=:userId")
    fun getContacts(userId: String): LiveData<List<Contact>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertContact(contact: Contact): Long

    @Delete
    fun deleteContact(contact: Contact)

    @Update
    fun updateContact(contact: Contact)

    @Transaction
    @Query("SELECT * FROM contact WHERE userId=:userId")
    fun getContactsWithItems(userId: String): LiveData<List<ContactWithContactItems>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertContactItem(contactItem: ContactItem): Long

    @Delete
    fun deleteContactItem(contactItem: ContactItem)

    @Update
    fun updateContactItem(contactItem: ContactItem): Int

}