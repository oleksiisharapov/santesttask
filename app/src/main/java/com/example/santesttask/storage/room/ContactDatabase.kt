package com.example.santesttask.storage.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.santesttask.model.Contact
import com.example.santesttask.model.ContactItem

@Database(entities = [Contact::class, ContactItem::class], version = 1)

abstract class ContactDatabase: RoomDatabase(){

    abstract fun contactDao(): ContactDAO

}