package com.example.santesttask.storage

import androidx.lifecycle.LiveData
import com.example.santesttask.model.Contact
import com.example.santesttask.model.ContactItem

interface Repository {

    suspend fun getContacts(): LiveData<List<Contact>>
    suspend fun addContact(contact: Contact)
    suspend fun updateContact(contact: Contact)
    suspend fun deleteContact(contact: Contact)
    suspend fun addContactItem(contactItem: ContactItem)
    suspend fun deleteContactItem(contactItem: ContactItem)

}