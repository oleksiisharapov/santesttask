package com.example.santesttask.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.santesttask.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.bottom_sheet_fragment.*

class BottomSheetContactInfoFragment: BottomSheetDialogFragment(), View.OnClickListener {

    interface BottomSheetDialogClickListener{
        fun itemClicked(id: Int)
    }

    private lateinit var clickListener: BottomSheetDialogClickListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bottom_sheet_fragment, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        clickListener = parentFragment as BottomSheetDialogClickListener

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bottom_sheet_phone.setOnClickListener(this)
        bottom_seheet_email.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        clickListener.itemClicked(p0!!.id)
        dismiss()
    }
}