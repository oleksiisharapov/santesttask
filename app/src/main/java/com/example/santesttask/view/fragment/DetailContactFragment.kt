package com.example.santesttask.view.fragment

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.santesttask.R
import com.example.santesttask.extensions.sendEmail
import com.example.santesttask.extensions.startCall
import com.example.santesttask.model.Contact
import com.example.santesttask.model.ContactItem
import com.example.santesttask.view.MainActivity
import com.example.santesttask.view.ProvideViewModelFactory
import com.example.santesttask.viewmodel.MainViewModel
import com.example.santesttask.viewmodel.MainViewModelFactory
import kotlinx.android.synthetic.main.detailed_contact_fragment.*

class DetailContactFragment : Fragment(), DialogInterface.OnClickListener {

    private val alertDialog by lazy {
        AlertDialog.Builder(context!!)
            .setMessage("Delete contact?")
            .setPositiveButton("Yes, delete", this)
            .setNegativeButton("No", this)
            .create()
    }

    private var contactInfoList = mutableListOf<ContactItem>()

    private lateinit var viewModel: MainViewModel
    private lateinit var viewModelFactory: MainViewModelFactory
    lateinit var contact: Contact


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.detailed_contact_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.getParcelable<Contact>("Contact")?.let {
            contact = it
        }
        contact.contactList?.let {
            contactInfoList = it as MutableList<ContactItem>
        }

        viewModelFactory = (activity as ProvideViewModelFactory).getViewModelFactory()
        viewModel = ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)

        userName.text = "${contact?.firstName} ${contact?.secondName}"

        contactInfoItems.adapter = ContactInfoItemAdapter(context!!, R.layout.contact_info_item_layout, contactInfoList)
        contactInfoItems.onItemClickListener = AdapterView.OnItemClickListener { _, _, p2, _ ->
            when (contactInfoList[p2].contactItemType) {
                "Phone" -> (activity as MainActivity).startCall(contactInfoList[p2].contactInfo)
                "Email" -> (activity as MainActivity).sendEmail(contactInfoList[p2].contactInfo, "")
            }
        }

        detailFragmentDeleteButton.setOnClickListener { alertDialog.show() }
        detailFragmentEditButton.setOnClickListener {
            val bundle = Bundle()
            bundle.putParcelable("ContactToEdit", contact)
            findNavController().navigate(R.id.action_detailContactFragment_to_addNewContactFragment, bundle)
        }
    }

    override fun onClick(p0: DialogInterface?, selectedOption: Int) {
        when (selectedOption) {
            DialogInterface.BUTTON_POSITIVE -> {
                contact?.let {
                    viewModel.deleteContact(it)
                    findNavController().navigateUp()
                }
            }
            DialogInterface.BUTTON_NEGATIVE -> {
                Toast.makeText(context, "No", Toast.LENGTH_LONG).show()
            }
        }
    }

    class ContactInfoItemAdapter(
        private val ctx: Context,
        private val layoutId: Int,
        private val list: List<ContactItem>) : BaseAdapter() {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

            val item = list[position]
            val view = LayoutInflater.from(ctx).inflate(layoutId, parent, false)
            val textView = view.findViewById(R.id.contactInfoTextView) as TextView
            val imageView = view.findViewById(R.id.contactInfoTextViewImage) as ImageView

            textView.text = item.contactInfo

            when (item.contactItemType) {
                "Email" -> imageView.setImageResource(R.drawable.ic_email)
                "Phone" -> imageView.setImageResource(R.drawable.ic_phone)
            }
            return view
        }

        override fun getItem(p0: Int): Any {
            return list[p0]
        }

        override fun getItemId(p0: Int): Long {
            return p0.toLong()
        }

        override fun getCount(): Int {
            return list.size
        }
    }

}
