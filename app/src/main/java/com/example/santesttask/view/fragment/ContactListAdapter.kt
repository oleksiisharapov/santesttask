package com.example.santesttask.view.fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.santesttask.R
import com.example.santesttask.model.Contact

class ContactListAdapter(
    private val listener: ContactListItemClickListener,
    private val contactList: List<Contact>
) : RecyclerView.Adapter<ContactListAdapter.ContactListViewHolder>() {

    private var expandedItem = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactListViewHolder {
        return ContactListViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.contact_item_layout,
                parent,
                false
            ),
            listener
        )
    }

    override fun getItemCount(): Int {
        return contactList.size
    }

    override fun onBindViewHolder(holder: ContactListViewHolder, position: Int) {
        holder.onBind(contactList[position])
    }

    inner class ContactListViewHolder(view: View, private val itemClickListener: ContactListItemClickListener) :
        RecyclerView.ViewHolder(view) {

        private var contactNameTextView: TextView = view.findViewById(R.id.contactItemName) as TextView

        fun onBind(item: Contact) {
            contactNameTextView.text = "${item.firstName} ${item.secondName}"
            itemView.setOnClickListener {
                itemClickListener.onItemClick(item)
            }
        }
    }
}

interface ContactListItemClickListener {
    fun onItemClick(item: Contact)
}
