package com.example.santesttask.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.room.Room
import com.example.santesttask.R
import com.example.santesttask.storage.DatabaseRepository
import com.example.santesttask.storage.Repository
import com.example.santesttask.storage.room.ContactDatabase
import com.example.santesttask.viewmodel.MainViewModelFactory
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), DrawerBehaviour, ProvideViewModelFactory,
    NavigationView.OnNavigationItemSelectedListener {

    val SIGN_IN_REQUEST_CODE = 6001

    lateinit var repository: Repository
    lateinit var mainViewModelFactory: MainViewModelFactory

    lateinit var navController: NavController
    lateinit var contactDb: ContactDatabase

    lateinit var navHeaderUserEmailTextView: TextView
    lateinit var navHeaderUsernameTextView: TextView

    lateinit var googleSignInClient: GoogleSignInClient
    val googleSignOptions: GoogleSignInOptions by lazy {
        GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        googleSignInClient = GoogleSignIn.getClient(this, googleSignOptions)

        val navHostFragment = mainNavHostFragment as NavHostFragment
        val graphInflater = navHostFragment.navController.navInflater

        val navGraph = graphInflater.inflate(R.navigation.nav_graph)
        navController = navHostFragment.navController


        navigationView.setNavigationItemSelectedListener(this)

        navHeaderUserEmailTextView = navigationView.getHeaderView(0).findViewById(R.id.navHeaderUserEmail)
        navHeaderUsernameTextView = navigationView.getHeaderView(0).findViewById(R.id.navHeaderUsername)

        contactDb = Room.databaseBuilder(applicationContext, ContactDatabase::class.java, "contactsDB")
            .fallbackToDestructiveMigration()
            .build()


        /*
          Conditional settings of navhostfragment
        */

        val client = GoogleSignIn.getLastSignedInAccount(this)
        if (client != null) {
            setUserCredentials(client.displayName!!, client.email!!)
            repository = DatabaseRepository(contactDb.contactDao(), client.id!!)
            mainViewModelFactory = MainViewModelFactory(repository)
            navGraph.startDestination = R.id.contactListFragment
        } else {
            navGraph.startDestination = R.id.loginFragment
        }
        navController.graph = navGraph

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SIGN_IN_REQUEST_CODE) {
                try {
                    val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                    val googleSignInAccount = task.result
                    setUserCredentials(googleSignInAccount?.displayName ?: "", googleSignInAccount?.email ?: "")
                    repository = DatabaseRepository(contactDb.contactDao(), googleSignInAccount!!.id!!)
                    mainViewModelFactory = MainViewModelFactory(repository)
                    navController.popBackStack()
                      navController.navigate(R.id.contactListFragment)
                } catch (e: ApiException) {
                    Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
                }
            }
        } else {
            Toast.makeText(this, "Something goes wrong. Check the network and try again", Toast.LENGTH_LONG).show()
        }
    }


    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        if (p0.itemId == R.id.menu_logout) {
            googleSignInClient.signOut().addOnCompleteListener {
                if (it.isComplete) {
                    navController.popBackStack()
                    navController.navigate(R.id.loginFragment)
                }
            }
        }
        return true
    }

    override fun disableDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
    }

    override fun enableDrawer() {
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
    }

    override fun getViewModelFactory(): MainViewModelFactory {
        return mainViewModelFactory
    }

    private fun setUserCredentials(userName: String, userEmail: String) {
        navHeaderUserEmailTextView.text = userEmail
        navHeaderUsernameTextView.text = userName
    }
}

/*

    The navDrawer behaviour interface

*/
interface DrawerBehaviour {
    fun disableDrawer()
    fun enableDrawer()
}


//Some weird but working implementation of DI
interface ProvideViewModelFactory {

    fun getViewModelFactory(): MainViewModelFactory
}