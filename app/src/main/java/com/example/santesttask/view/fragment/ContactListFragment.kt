package com.example.santesttask.view.fragment

import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.santesttask.R
import com.example.santesttask.model.Contact
import com.example.santesttask.view.DrawerBehaviour
import com.example.santesttask.view.ProvideViewModelFactory
import com.example.santesttask.viewmodel.MainViewModel
import com.example.santesttask.viewmodel.MainViewModelFactory
import kotlinx.android.synthetic.main.contact_list_layout.*


class ContactListFragment : Fragment(), ContactListItemClickListener, CompoundButton.OnCheckedChangeListener {


    lateinit var viewModelFactory: MainViewModelFactory
    lateinit var viewModel: MainViewModel

    val contactList = mutableListOf<Contact>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.contact_list_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as DrawerBehaviour).enableDrawer()

        viewModelFactory = (activity as ProvideViewModelFactory).getViewModelFactory()
        viewModel = ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)

        viewModel.getContacts()

        contactListRecyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        contactListRecyclerView.adapter = ContactListAdapter(this, contactList)
        contactListRecyclerView.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                outRect.apply {
                    left = 10
                    right = 10
                    top = 6
                    bottom = 6
                }
            }
        })
        contactListFAB.setOnClickListener {
            findNavController().navigate(
                R.id.action_contactListFragment_to_addNewContactFragment,
                null,
                null,
                FragmentNavigatorExtras(contactListFAB to "sharedElement")
            )
        }

        sortByNameChip.setOnCheckedChangeListener(this)
        sortByNameAmount.setOnCheckedChangeListener(this)
    }

    override fun onResume() {
        super.onResume()

        viewModel.contactList.observe(this, Observer {
            contactList.clear()
            contactList.addAll(it)

            contactListRecyclerView.adapter?.notifyDataSetChanged()
            if (it.size > 1) {
                chipContainer.visibility = View.VISIBLE
            } else {
                chipContainer.visibility = View.GONE
            }
        })
    }

    override fun onItemClick(item: Contact) {
        val bundle = Bundle()
        bundle.putParcelable("Contact", item)
        findNavController().navigate(R.id.action_contactListFragment_to_detailContactFragment, bundle)
    }

    override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
        if (p1) {
            when (p0?.id) {
                sortByNameChip.id -> {
                    contactList.sortBy { it.firstName }
                    contactListRecyclerView.adapter?.notifyDataSetChanged()
                }
                sortByNameAmount.id -> {
                    contactList.sortByDescending { it.contactList?.size }
                    contactListRecyclerView.adapter?.notifyDataSetChanged()
                }
            }
        }
    }

}