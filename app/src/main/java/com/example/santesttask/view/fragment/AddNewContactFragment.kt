package com.example.santesttask.view.fragment

import android.os.Bundle
import android.text.InputType
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.santesttask.R
import com.example.santesttask.model.Contact
import com.example.santesttask.model.ContactItem
import com.example.santesttask.view.ProvideViewModelFactory
import com.example.santesttask.viewmodel.MainViewModel
import com.example.santesttask.viewmodel.MainViewModelFactory
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.add_new_contact.*


class AddNewContactFragment : Fragment(), BottomSheetContactInfoFragment.BottomSheetDialogClickListener {

    private val rowList = mutableListOf<Pair<Int, String>>()

    private val resultList = mutableListOf<ContactItem>()

    private val bottomSheetFragment by lazy { BottomSheetContactInfoFragment() }

    lateinit var viewModelFactory: MainViewModelFactory
    lateinit var viewModel: MainViewModel

    var contactToEdit: Contact? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        return inflater.inflate(R.layout.add_new_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addContactInfoButton.setOnClickListener {
            bottomSheetFragment.show(childFragmentManager, bottomSheetFragment.tag)
        }

        addnewContactButton.setOnClickListener { addNewContact() }

        contactToEdit = arguments?.getParcelable<Contact>("ContactToEdit")
        contactToEdit?.let {

            addNewContactFirstNameEditText.setText(it.firstName)
            addNewContactSecondNameEditText.setText(it.secondName)
            addnewContactButton.setText("ApplyChanges")
            addNewContactTitle.setText("Edit contact info")

            it.contactList?.forEach { item ->
                resultList.add(item)
                addContactInfoRow(
                    item.contactItemType,
                    when (item.contactItemType) {
                        "Email" -> InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                        "Phone" -> InputType.TYPE_CLASS_PHONE
                        else -> return
                    },
                    item
                )
            }
            addnewContactButton.setOnClickListener { updateContact() }
        }

        viewModelFactory = (activity as ProvideViewModelFactory).getViewModelFactory()
        viewModel = ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)

    }

    override fun onResume() {
        super.onResume()
        startPostponedEnterTransition()
    }

    override fun itemClicked(id: Int) {
        when (id) {
            R.id.bottom_seheet_email -> addContactInfoRow("Email", InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS)
            R.id.bottom_sheet_phone -> addContactInfoRow("Phone", InputType.TYPE_CLASS_PHONE)
        }
    }

    private fun addContactInfoRow(hint: String, inputType: Int, initValue: ContactItem? = null) {
        val layoutInflater = LayoutInflater.from(context)
        val view = layoutInflater.inflate(R.layout.cantact_info_edit_row, addNewContactRoot, false)
        view.id = initValue?.contactItemId ?: View.generateViewId()

        val edittextout = view as TextInputLayout
        val editText = edittextout.editText
        editText?.inputType = inputType
        edittextout.hint = hint
        initValue?.let {
            editText?.setText(it.contactInfo)
        }
        addNewContactRoot.addView(view)
        rowList.add((edittextout.id to hint))
    }


    private fun addNewContact() {
        rowList.forEach{
            val edittext = (view?.findViewById<TextInputLayout>(it.first))?.editText
            val str = edittext?.text.toString()
            if (str != "") {
                    resultList.add(ContactItem(0, -1, str, it.second))
                }
            }
        viewModel.addContact(
            Contact(
                0,
                addNewContactFirstNameEditText.text.toString(),
                addNewContactSecondNameEditText.text.toString(),
                "",
                resultList
            )
        )
        findNavController().navigateUp()
    }

    private fun updateContact() {
        rowList.forEachIndexed { index, pair ->
            val edittext = (view?.findViewById<TextInputLayout>(pair.first))?.editText
            val str = edittext?.text.toString()
            if (str != "") {
                if(index < resultList.size){
                    resultList[index].contactInfo = str
                }else {
                    resultList.add(ContactItem(0, contactToEdit?.id!!, str, pair.second))
                }
            }
        }
        viewModel.updateContact(
            Contact(
                contactToEdit?.id!!,
                addNewContactFirstNameEditText.text.toString(),
                addNewContactSecondNameEditText.text.toString(),
                contactToEdit?.userId!!,
                resultList
            )
        )
        findNavController().navigate(R.id.action_addNewContactFragment_to_contactListFragment)
    }

}