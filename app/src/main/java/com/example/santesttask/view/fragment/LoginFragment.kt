package com.example.santesttask.view.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.santesttask.R
import com.example.santesttask.view.DrawerBehaviour
import com.example.santesttask.view.SplashActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import kotlinx.android.synthetic.main.login_layout.*
import kotlinx.android.synthetic.main.navdrawer_header.*

class LoginFragment : Fragment(), View.OnClickListener {

    val SIGN_IN_REQUEST_CODE = 6001

    val googleSignOptions: GoogleSignInOptions by lazy {
        GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()
    }

    lateinit var googleSignInClient: GoogleSignInClient

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.login_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        googleSignInClient = GoogleSignIn.getClient(context!!, googleSignOptions)

        loginGoogleButton.setOnClickListener(this)
        (activity as DrawerBehaviour).disableDrawer()
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.loginGoogleButton -> {
                val account = GoogleSignIn.getLastSignedInAccount(context)
                if (account == null) {
                    //start an google sign in activity from parent activity
                    activity?.startActivityForResult(googleSignInClient.signInIntent, SIGN_IN_REQUEST_CODE)
                }
            }
        }
    }

}