package com.example.santesttask.view

import android.animation.Animator
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewAnimationUtils
import androidx.core.animation.addListener
import com.example.santesttask.R
import kotlinx.android.synthetic.main.splash_activity.*

class SplashActivity : AppCompatActivity() {

    private var animator: Animator? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)
        buttonStart.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java).apply {
                flags =
                    Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            })
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        buttonStart.post {
            animator = ViewAnimationUtils.createCircularReveal(
                buttonStart,
                buttonStart.width / 2,
                buttonStart.height / 2,
                0f,
                100f
            ).apply {
                duration = 2000L
                interpolator = android.view.animation.AccelerateInterpolator(2.5f)
                addListener({ buttonStart.isClickable = true }, {}, {}, {})
            }
            buttonStart.visibility = View.VISIBLE
            buttonStart.isClickable = false
            animator?.start()
        }
    }


}
