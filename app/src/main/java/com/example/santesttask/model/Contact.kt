package com.example.santesttask.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize


@Parcelize
@Entity(tableName = "contact")
data class Contact(

    @PrimaryKey(autoGenerate = true)
    var id: Int,

    var firstName: String,

    var secondName: String,

    var userId: String,

    @Ignore
    var contactList: List<ContactItem>?

) : Parcelable {
    constructor() : this(0, "", "", "", null)
}