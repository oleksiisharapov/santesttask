package com.example.santesttask.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize


@Parcelize
@Entity(tableName = "contact_item")
data class ContactItem(
    @PrimaryKey(autoGenerate = true)
        var contactItemId: Int,
    var contactId: Int,
    var contactInfo: String,
    var contactItemType: String
): Parcelable