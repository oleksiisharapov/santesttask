package com.example.santesttask.model

import androidx.room.Embedded
import androidx.room.Relation

data class ContactWithContactItems(
    @Embedded
    var contact: Contact,

    @Relation(parentColumn = "id", entityColumn = "contactId", entity = ContactItem::class)
    var contactItems: List<ContactItem>

)