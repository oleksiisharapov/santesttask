package com.example.santesttask.viewmodel

import androidx.lifecycle.*
import com.example.santesttask.model.Contact
import com.example.santesttask.storage.Repository
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class MainViewModel(private val repository: Repository): ViewModel(){

    var contactList = MediatorLiveData<List<Contact>>()

    fun getContacts() {
       viewModelScope.launch {
           contactList.addSource(repository.getContacts()) {
               contactList.value = it
           }
       }
    }

    fun addContact(contact: Contact){
        viewModelScope.launch(IO) {
            repository.addContact(contact)
        }
    }

    fun deleteContact(contact: Contact){
        viewModelScope.launch(IO) {
            repository.deleteContact(contact)
        }
    }

    fun updateContact(contact: Contact){
        viewModelScope.launch(IO){
            repository.updateContact(contact)
        }
    }


}